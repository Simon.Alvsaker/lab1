package INF101.lab1.INF100labs;


/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        
        // Triple if-statement and ">=" accounts for if some of them are equal
        // If word1 is the longest word
        if (word1.length() >= word2.length() && word1.length() >= word3.length()) {
            System.out.println(word1);
        } 

        // If word2 is the longest word
        if (word2.length() >= word1.length() && word2.length() >= word3.length()) {
            System.out.println(word2);
        } 

        // If word3 is the longest word
        if (word3.length() >= word1.length() && word3.length() >= word2.length()) {
            System.out.println(word3);
        } 
    }

    public static boolean isLeapYear(int year) {
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num % 2 == 0) {return true;}
        else {return false;}
    }

}
