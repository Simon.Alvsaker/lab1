package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> multipliedList = new ArrayList<>();
        for (int num : list) {
            multipliedList.add(num*2);
        }
        return multipliedList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> removedThreesList = new ArrayList<>();
        for (int num : list) {
            if (num != 3) {
                removedThreesList.add(num);
            }
        }
        return removedThreesList;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        // HashSet filters out the unique values
        ArrayList<Integer> uniqueList = new ArrayList<>(new HashSet<>(list));

        // A more manually implemented solution
        ArrayList<Integer> uniqueList2 = new ArrayList<>();
        for (int num : list) {
            if (!uniqueList2.contains(num)) {
                uniqueList2.add(num);
            }
        }
        // return uniqueList2;
        return uniqueList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int num : b) {
            int indexOfnum = b.indexOf(num);
            a.set(indexOfnum, num + a.get(indexOfnum));
        }
    }

}