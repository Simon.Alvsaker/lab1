package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        ArrayList<Integer> summedRowsList = new ArrayList<>();
        ArrayList<Integer> summedColumnsList = new ArrayList<>();

        // Loops through every row
        for (int row = 0; row < grid.size(); row++) {
            // Loops through every row and sums the numbers and adds it to a list, 
            //sums the first number of each row to a list of column sums as well
            int sumRow = 0;
            for (int i = 0; i < grid.get(row).size(); i++) {
                // adds the number at index i of the current row
                int num = grid.get(row).get(i);
                sumRow += num;
                // First column adds the elements
                if (row == 0) {
                    summedColumnsList.add(num);
                }
                // If not the first row, it sums the number to the element at the respective index
                else {
                    summedColumnsList.set(i, summedColumnsList.get(i) + num);
                }
            }
            summedRowsList.add(sumRow);
        }
        // Checks if the sum of all rows are equal, and if the sum of all columns are equal
        int firstNumRows = summedRowsList.get(0);
        int firstNumColumns = summedColumnsList.get(0);
        boolean rowsEqual = true;
        boolean columnsEqual = true;
        for (int sum : summedRowsList) {
            if (sum != firstNumRows) {
                rowsEqual = false;
            }
        }
        for (int sum : summedColumnsList) {
            if (sum != firstNumColumns) {
                columnsEqual = false;
            }
        }

        // Checks if both are true
        if (rowsEqual && columnsEqual) {
            return true;
        }
        else {
            return false;
        }
    }

}